var timeout = 100;
var clearPeriodDetailsTimeout;
var clearTimeDetailsTimeout;

$(function() {
    $('#s_results_tablebody > tr').each(function() {
        //-----------------------------------------------------------
        // show/hide approved and not approved projects and approvers
        //-----------------------------------------------------------

        // retrieving url for getting approvers details
        var ancor = $(this).children('td').last().find('a').first();
        var onclk = ancor.attr("onclick");
        onclk = onclk.substring(onclk.indexOf("'") + 1);

        var approversUrl = "https://ets.endava.com/jtcgi/" + onclk.substring(0, onclk.indexOf("'"));
        var periodTD = $(this).children('td')[3];

        $.get(approversUrl, function(data) {
                var ready = false;

                var res = String(data);

                var startIndex = res.indexOf("Project Approval Information");

                if (startIndex == -1) {
                    ready = true;
                } else {
                    var awaitingIndex = res.indexOf("Awaiting Approval", startIndex);
                    var ready = awaitingIndex == -1;
                }

                if (ready) {
                    //$(periodTD).attr("style", "background-color: green;");
                    $(periodTD).children('div').first().children('a').first().attr("style", "color: green;");
                } else {
                    $(periodTD).children('div').first().children('a').first().attr("style", "color: red;");
                }

                var approversDataTable = $('<table>');
                approversDataTable.attr("id", "approversDataTable");
                approversDataTable.attr("class", "clsSearchResultsTable contentTable");
                approversDataTable.css("background-color", "gainsboro");

                // collect information approval status for each approver
                var tables = $("table", res);
                for (var i = 0; i < tables.length; i++) {
                    var table = $(tables[i]);
                    if (table.html().indexOf("Approver Login") > 0 && table.html().indexOf("Status") > 0) {
                        var caption = $($("h3", table)[0]).text();
                        caption = caption.substring(caption.indexOf("\"") + 1);
                        caption = caption.substring(0, caption.indexOf("\""));

                        approversDataTable.append($('<tr style="color:black;font-weight:bold;"><td colspan="2">' + caption + '</td></tr>'));

                        var trs = $("tr", table);

                        for (var j = 0; j < trs.length; j++) {
                            var tr = $(trs[j]);

                            var tds = $("td", tr);
                            var status = $(tds[tds.length-1]).text();
                            var color = status == "Approved" ? "green" : "red";
                            approversDataTable.append(
                                $('<tr>')
                                    .append(tds[1])
                                    .append(tds[tds.length-1])
                                    .attr("style", "color:" + color + ";")
                            );
                        }
                    }
                }

                $(periodTD).mouseover(function() {
                    clearPeriodDetails();
                    clearTimeDetails();

                    var pos = $(periodTD).position();

                    approversDataTable.css("position", "absolute")
                        .css("left", (pos.left + 180))
                        .css("z-index", "9999");
                    $(document.body).append(approversDataTable);

                    var delta = Math.max(0, (pos.top + approversDataTable.height()) - ($(window).scrollTop() + $(window).height() - 5));
                    approversDataTable.css("top", pos.top - delta);
                    approversDataTable.find('td').mouseleave(function(event) {
                        if (isMouseOutOfBoxes(event, $(periodTD), approversDataTable)) {
                            clearPeriodDetails();
                        }
                    });

                    // decorate row
                    colorTableRowByTd(periodTD);
                });

                $(periodTD).mouseout(function(event) {
                    if (isMouseOutOfBoxes(event, $(periodTD), approversDataTable)) {
                        clearPeriodDetails();
                    }
                });
            }
        );

        //-----------------------------------------
        // show/hide time details------------------
        //-----------------------------------------

        // retrieving url for getting time details
        var tancor = $($(this).children('td')[3]).find('a').first();
        var tonclk = tancor.attr("onclick");
        tonclk = tonclk.substring(tonclk.indexOf("'") + 1);

        var timedetailsUrl = "https://ets.endava.com/jtcgi/" + tonclk.substring(0, tonclk.indexOf("'"));

        var timeTD = $(this).children('td')[4];
        var span = $('<a>')
            .attr('onclick', tancor.attr("onclick"))
            .attr('href', tancor.attr("href"))
            .html($(timeTD).html())
            .css('cursor', 'pointer').css('text-decoration', 'underline');
        $(timeTD).html('').append(span);

        $.get(timedetailsUrl, function(data) {
                var res = String(data);

                var timedetailsTable = $('<table>');
                timedetailsTable.attr("id", "timedetailsTable");
                timedetailsTable.attr("class", "clsSearchResultsTable contentTable");
                timedetailsTable.css('width', '440px');

                // collect information approval status for each approver
                var tableLeftbody = $("#idMainTableLeftbody", res);
                var totalColsbody = $("#idTotalColsbody", res);

                var nameTRrs = $("tr", tableLeftbody);
                var totalTRrs = $("tr", totalColsbody);

                for (var j = 1; j < nameTRrs.length; j++) {
                    var tr = $(nameTRrs[j]);

                    var projectName = $("#showmeh1_" + (j-1), tr).html();

                    var taskNameContainer = $("#idMainTableLeftrow" + j + "column1", tr).find('div.clsFieldValueCell');
                    taskNameContainer.children('input').remove();
                    var taskName = taskNameContainer.html();

                    var totalHrsContainer = $("#idTotalColsrow" + j + "column0", totalColsbody);
                    var totalHrs = totalHrsContainer.find('input').val();

                    var bilabilityContainer = $('#idMainTableLeftrow' + j + 'column2', tr).find('div.clsFieldValueCell');
                    bilabilityContainer.children('input').remove();
                    var bilability = bilabilityContainer.html();
                    bilability = bilability.replace("1. ","").replace("2. ","").replace("3. ","").replace("4. ","");

                    timedetailsTable.append(
                        $('<tr>')
                            .append(
                            $('<td>')
                                .append(
                                $('<span>')
                                    .html(projectName)
                                    .css('font-weight', 'bold')
                            )
                                .append('<br/>')
                                .append(
                                $('<span>')
                                    .attr('style', 'color: darkred; font-size: 10px;')
                                    .html(
                                        taskName + "&nbsp;" +
                                        $('<span>')
                                            .attr('style', 'color: darkgray; font-size: 10px;')
                                            .html('(' + bilability + ')')[0].outerHTML
                                )
                            ).attr('style', 'padding: 5px; border-width: 1px;border-color: #C26700;background-color: gainsboro; border-right-style: none;')
                        ).append(
                            $('<td>')
                                .html(totalHrs)
                                .attr('style', 'border-width: 1px;' +
                                    'border-color: #C26700;' +
                                    'background-color: gainsboro;' +
                                    'text-align: right;' +
                                    'border-left-style: none;' +
                                    'font-weight: bold;' +
                                    'vertical-align: top;' +
                                    'padding-top: 5px;')
                        )
                    );
                }

                $(timeTD).mouseover(function() {
                    clearPeriodDetails();
                    clearTimeDetails();

                    var pos = $(timeTD).position();
                    timedetailsTable.css("position", "absolute")
                        .css("left", (pos.left + 75))
                        .css("z-index", "9999");
                    $(document.body).append(timedetailsTable);

                    var delta = Math.max(0, (pos.top + timedetailsTable.height()) - ($(window).scrollTop() + $(window).height() - 5));
                    timedetailsTable.css("top", pos.top - delta);
                    timedetailsTable.find('td').mouseleave(function(event) {
                        if (isMouseOutOfBoxes(event, $(timeTD), timedetailsTable)) {
                            clearTimeDetails();
                        }
                    });

                    // decorate row
                    colorTableRowByTd(timeTD);
                });

                $(timeTD).mouseout(function(event) {
                    if (isMouseOutOfBoxes(event, $(timeTD), timedetailsTable)) {
                        clearTimeDetails();
                    }
                });
            }
        );
    });
});

function isMouseOutOfBoxes(event, td, table) {
    var mx = event.pageX;
    var my = event.pageY;

    var isInTd = isIn(mx, my, td);
    var isInTable = isIn(mx, my, table);

    return !(isInTd || isInTable);
}

function isIn(x, y, element) {
    var ex = element.position().left;
    var ey = element.position().top;
    var ew = ex + element.width();
    var eh = ey + element.height();

    return x >= ex && x <= ew && y >= ey && y <= eh;
}

function clearPeriodDetails() {
    $('#approversDataTable').remove();
    removeTableTrStyles();
}

function clearTimeDetails() {
    $('#timedetailsTable').remove();
    removeTableTrStyles();
}

function colorTableRowByTd(td) {
    removeTableTrStyles();

    $(td).parent().children('td').css('background-color','gainsboro');
}

function removeTableTrStyles() {
    $('#s_results_tablebody > tr').children('td').attr('style', '');
}