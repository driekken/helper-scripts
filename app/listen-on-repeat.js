/*jshint strict:false, browser:true */

/*
This small bookmarklet is intended to easily switch a Youtube song to ListenOnRepeat
avoiding the need of having to explicitly type out the URL.
 */

(function bookmarklet() {
    var domain = document.domain;
    var newDomain = 'www.listenonrepeat.com';

    var url = document.URL;
    var newUrl = url.replace(domain, newDomain);

    location.href = newUrl;
}());
