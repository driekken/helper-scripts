/*jshint strict:false, browser:true */
(function () {
    var jQuery = require('jquery');

    var totalMinutes = 0;
    var totalSeconds = 0;

    jQuery('.module td.duration').each(
        function (index, elem) {
            var item = jQuery(elem).html().trim();
            var regex = /^.*:(.*):(.*)$/;
            var match = regex.exec(item);
            totalMinutes += parseInt(match[1]);
            totalSeconds += parseInt(match[2]);
        }
    );

    var rest = totalSeconds % 60;
    var msg = 'total length : [minutes = ' + (totalMinutes + (totalSeconds - rest) / 60) + ', seconds = ' + rest + ']';

    alert(msg);
})();


