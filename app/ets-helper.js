/*jshint strict:false, browser:true */

(function () {
    var jqueryScript = document.createElement('script');
    jqueryScript.type = 'text/javascript';
    jqueryScript.src = '//code.jquery.com/jquery-1.9.0.min.js';

    var headID = document.getElementsByTagName('head')[0];
    headID.appendChild(jqueryScript);

    setTimeout(function () {
        var newScript = document.createElement('script');
        newScript.type = 'text/javascript';
        newScript.src = 'https://dl.dropboxusercontent.com/s/7kpy6ryin0hra90/ets2.js?dl=1&token_hash=AAGEcDUwH2OJxjG0t-eTPGEMRL3bgJ9ZkKEKnkZauyK7QQ';
        headID.appendChild(newScript);
    }, 1000);
})();