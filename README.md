# Helper-scripts
> This repository will host various scripts to be used as bookmarklets

All the scripts have to be located in the app/ folder.

When ready, execute the following JavaScript instruction in the top-level folder:

    :::javascript
    gulp

You can then find the resulting bookmarklets in the dist/ folder.