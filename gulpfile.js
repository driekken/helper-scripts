'use strict';

var gulp = require('gulp');
var gulpClean = require('gulp-clean');
var gulpConcat = require('gulp-concat');
var gulpJshint = require('gulp-jshint');
var gulpUglify = require('gulp-uglify');
var gulpForEach = require('gulp-foreach');
var gutil = require('gulp-util');
var jshintStylish = require('jshint-stylish');
var buffer = require('buffer');
var path = require('path');
var map = require('map-stream');

var paths = ['app/{,*/}*.js'];

gulp.task('generate', function () {
    var header = new Buffer('// Copy this to your URL bar:\njavascript:');

    gulp.src(paths)
        .pipe(gulpForEach(function (stream, file) {
            return stream
                .pipe(gulpJshint({devel: true}))
                .pipe(gulpJshint.reporter(jshintStylish))
                .pipe(gulpUglify())
                .pipe(gulpConcat(path.basename(file.path)))
                .pipe(map(function (file, cb) {
                    file.contents = buffer.Buffer.concat([header, file.contents]);
                    cb(null, file);
                }));

        }))
        .pipe(gulp.dest('dist/'));
});

gulp.task('clean', function () {
    gulp.src('dist').pipe(gulpClean());
});

gulp.task('default', ['clean', 'generate']);

gulp.task('watch', function () {
    gulp.watch(paths, ['generate'])
});
